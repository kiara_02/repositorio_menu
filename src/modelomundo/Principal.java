
package modelomundo;

import java.util.Scanner;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;

public class Principal {

	static Scanner entrada = new Scanner(System.in);

	public static void main(String[] args) throws Exception {

		//declara variables para poder pedir al usuario los datos del empleado e ingresar

		Empleado empleado1 = null;


		//DECLARAR VARIABLES
		String nombreEmple = null;
		String apellidoEmple = null;
		int genero=0; // 1 Femenino || 2 Masculino
		String imagen;
		double salario=0;

		int dia=0, mes=0 ,anio=0;
		Fecha fechaNac;
		Fecha fechaIngreso;

		Scanner sn = new Scanner(System.in);
		int opcion=0;

		//OPCIONES SISTEMA  1.INGRESAR DATOS  2.CALCULAR LA EDAD   3.CALCULAR LA ANTIGUEDAD DEL EMPLEADO EN LA EMPRESA
		//4.CALCULAR PRESTACIONES  5.VISUALIZAR LA INFORMACION
		do {
			
			System.out.println("            MENU  \n");
			System.out.println("1. Ingresar datos del empleado");
			System.out.println("2. Calcular la edad del empleado");
			System.out.println("3. Calcular la antiguedad del empleado en la empresa");
			System.out.println("4. Calcular prestaciones del empleado");
			System.out.println("5. Visualizar la informacion del empleado");
			System.out.println("6. Salir");
			
			
			System.out.println("\nEscoja una de las opciones");
			try {
			opcion = sn.nextInt();
			}
			catch(InputMismatchException e) { 
				System.out.println("Ingresar solo numeros");
				opcion=6;
				//e.printStackTrace();
            	}
			
            
			switch(opcion){
			case 1:
				System.out.println("Has seleccionado la opcion ingresar datos del empleado");
				
				int valorASCII=0;
				int cont=0;
				int j=0;
				boolean continua;
				
					
				for (j=0;j<=cont; j++) {
					if(cont>0) {
						cont=0;
					}
				System.out.println("Ingrese su Nombre:");
				nombreEmple= entrada.nextLine();
				for(int i=0;i < nombreEmple.length();i++) {
					char caracter = nombreEmple.charAt(i);
					valorASCII = (int) caracter;
					if (valorASCII <97 || valorASCII > 122) {
						cont++;
					}
				}
				if (cont==0)break;
					System.out.println("\nIngrese solo letras\n");
			}
				
				
				for (j=0;j<=cont; j++) {
				
					if(cont>0) {
						cont=0;
					}
				System.out.println("Ingrese su Apellido:");
				apellidoEmple= entrada.nextLine();
				for(int i=0;i < apellidoEmple.length();i++) {
					char caracter = apellidoEmple.charAt(i);
					valorASCII = (int) caracter;
					if (valorASCII <97 || valorASCII > 122) {
						cont++;
					}
				}
				if (cont==0)break;
					System.out.println("\nIngrese solo letras\n");
			}
			
			
				do {
				try {
				System.out.println("Ingrese su genero ||  1-Femenino   2-Masculino");
				genero= entrada.nextInt();
			    }catch(InputMismatchException e) {
				System.out.println("Ingresar solo numeros entre 1 y 2");	
			    genero=0;
			    entrada.nextLine();
			    }
				}while(genero<1 || genero>2);
			
				do {
				try {
					
				System.out.println("Ingrese su Salario");
				salario= entrada.nextDouble();
			    }catch(NumberFormatException e) {
				System.out.println("Ingrese numeros con decimales");
			
				entrada.nextLine();
			    }
				}while(salario<100);
				
				do {
			     try {
			    	 continua=false;
				System.out.println("Ingrese su dia de Nacimiento:");
				dia= entrada.nextInt();
			    }catch(InputMismatchException e) {
					System.out.println("El dia esta fuera del rango de 1 hasta 31");
					entrada.next();
					continua = true;
			    }
			    }while(dia<0 | dia >32);
				
				System.out.println("Ingrese su mes de Nacimiento:");
				mes= entrada.nextInt();
				System.out.println("Ingrese su a�o de Nacimiento:");
				anio= entrada.nextInt();
				
			    if  (mes<0 | mes>13) {
			    	throw new Exception ("El a�o solo tiene 12 meses");
			    }
			    if  (anio<0) {
			    	throw new Exception ("El a�o esta mal escrito");
			    }
				fechaNac= new Fecha (dia, mes,anio);
				
				System.out.println("Ingrese su dia de Ingreso:");
				dia= entrada.nextInt();
				System.out.println("Ingrese su mes de Ingreso:");
				mes= entrada.nextInt();
				System.out.println("Ingrese su a�o de Ingreso:");
				anio= entrada.nextInt();
				fechaIngreso= new Fecha (dia, mes,anio);
				if  (dia<0 | dia >32) {
			    	throw new Exception ("El dia esta fuera del rango de 1 hasta 31");
			    }
			    if  (mes<0 | mes>13) {
			    	throw new Exception ("El a�o solo tiene 12 meses");

			    }
			    if  (anio<0) {
			    	throw new Exception ("El a�o esta mal escrito");

			    }
			    
                empleado1=new Empleado(nombreEmple, apellidoEmple, genero,"", salario, fechaNac, fechaIngreso);
				break;

			case 2:
				System.out.println("\nHas seleccionado la opcion calcular la edad del empleado\n");
				
                try {
				System.out.println("Su edad es:" +empleado1.calcularEdad()+"\n");
			    }catch(ArithmeticException e) {
                System.out.println	("Excepci�n Aritm�tica");
                }
				break;
			case 3:
				System.out.println("\nHas seleccionado la opcion calcular la antiguedad del empleado en la empresa\n");
				
				try {

				System.out.println("Su antiguedad en la empresa es:" +empleado1.calcularAntiguedad()+"\n");
			    }catch(ArithmeticException e) {
                System.out.println	("Excepci�n Aritm�tica");
                }
				break;
			case 4:
				System.out.println("\nHas seleccionado la opcion calcular las prestaciones del empleado\n");
				
                try {
				System.out.println("Sus prestaciones son:" +empleado1.calcularPrestaciones()+"\n");
                }catch(ArithmeticException e) {
                System.out.println	("Excepci�n Aritm�tica");
                }
				break;
			case 5:
				System.out.println("\nHas seleccionado la opcion visualizar la informaci�n del empleado\n");

				empleado1.mostrarDatos();

				break;
			case 6:
				System.out.println("\nSalir del programa");
                System.exit(0);
				break;

			default:
				System.out.println("Opcion Invalida");
			}
		}while(opcion!=6);
	
	}
}
	




