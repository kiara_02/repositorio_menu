  
   package modelomundo;

   public class Fecha {
   private int dia;
   private int mes;
   private int anio;

//constructor

   public Fecha (int pDias, int pMes, int pAnio) {
	
   dia= pDias;
   mes= pMes;
   anio= pAnio;
   }
   
   public Fecha () {
		
	   dia= 0;
	   mes= 0;
	   anio= 0;
	   }
// metodos analizadores getters

   public int  getDia() {
	return dia;
	
   }
   public int getMes() {
	return mes;
	
   }
   public int getAnio() {
	return anio;
   }

 

  //Metodos funcionales
  //fecha1 23/04/2000   fecha2 15/08/2008
      public int darDiferenciaEnMeses (Fecha pFecha) {
	  
	  int numeroMeses = 0;

      int difAnios = pFecha.getAnio( );
      int difMeses = pFecha.getMes( );
      int difDias = pFecha.getDia( );

      // Calcula la diferencia en meses
      numeroMeses = 12 * ( difAnios - anio ) + ( difMeses - mes );

      // Si el d�a no es mayor, resta un mes
      if( difDias < dia )
      {
          numeroMeses--;
      }

      return numeroMeses;
      }
  

  //dd-mm-aaaa
  public String toString() {
	return dia + "-"+ mes +"-"+ anio;
  }
  }
